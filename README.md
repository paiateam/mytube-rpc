# MyTube RPC #

Client and Server applications using Remote Procedure Calls for their communication. Part of a practice of Distributed Computing course (Computing Engineering at [Universitat de Lleida](http://udl.cat/ca/en/))

## Project Dependencies ##

In order to use this project you will need:

* mytube-WS project (You can found this [here](https://bitbucket.org/paiateam/mytube-ws))
* Java 8
