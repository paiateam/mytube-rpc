package client;

import org.omg.CosNaming.NamingContextPackage.NotFound;
import utilities.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Client {

    private static ServiceProvider serviceProvider;
    private static String clientID;
    private static File downloadFolder = new File("downloads");

    public static void main(String[] args) {
        // <Server IP> <Server port>
        //System.setSecurityManager(new SecurityManager());
        if (args.length != 1) {
            System.out.println("Usage: ./Client <Server Address>");
            System.exit(2);
        }
        String registryUrl = args[0];

        try {
            serviceProvider = (ServiceProvider) Naming.lookup(registryUrl);
            mainLoop();
        } catch (Exception ex) {
            System.out.println(ex.toString());  //debug
            System.out.println("ERROR: The server is offline.");
            System.exit(-1);
        }
    }

    private static void mainLoop() {
        System.out.println(">>> Welcome to MyTube client!");
        System.out.println(">>> Enter your username:");
        System.out.print("€ ");
        InputStreamReader is = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(is);
        try {
            // User log in
            String name = br.readLine();
            System.out.println(">>> Enter your password:");
            System.out.print("€ ");
            String hashedPassword = hashPassword(readPassword());
            Response response = serviceProvider.logIn(name, hashedPassword);
            hashedPassword = ""; // Erase the password information
            if(response.isError()){
                System.out.println("Error: " + response.getMessage());
                System.exit(-1);
            }
            clientID = response.getMessage();
            // Create the downloads folder
            if (downloadFolder.mkdirs() || downloadFolder.isDirectory()) {
                System.out.println(">>> Created the downloads folder in " + downloadFolder.getAbsolutePath());
            } else {
                System.out.println("ERROR! Downloads folder can not be created!");
                System.exit(-1);
            }
            boolean end = false;
            while (!end) {
                printActions();
                String action = br.readLine();
                end = processAction(action, br);
            }
            System.exit(0);
        } catch (IOException ex) {
            System.out.println("ERROR: Could not read from terminal");
        }
    }

    private static String readPassword() {
        Console console = System.console();
        if (console == null) {
            System.out.println("ERROR! Can not read the password. Please try to run this in a standard terminal.");
            System.exit(-2);
        }
        char[] password = console.readPassword();
        return new String(password);
    }

    private static String hashPassword(String password) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] passwordBytes = password.getBytes();
            byte[] hashedPasswordBytes = messageDigest.digest(passwordBytes);
            final StringBuilder builder = new StringBuilder();
            for(byte b : hashedPasswordBytes) {
                builder.append(String.format("%02x", b));
            }
            return builder.toString();
        } catch (NoSuchAlgorithmException e) {
            System.out.println("ERROR! Problems while hashing the password!");
            System.exit(-2);
        }
        return null;
    }

    private static void printActions() {
        System.out.println(">>> Enter the name of the action to be done: ");
        System.out.println(">>> - Upload");
        System.out.println(">>> - Download");
        System.out.println(">>> - Modify");
        System.out.println(">>> - Delete");
        System.out.println(">>> - Search by title (title)");
        System.out.println(">>> - Search by user (user)");
        System.out.println(">>> - Search by user and title (user-title)");
        System.out.println(">>> - Exit");
        System.out.print("€ ");
    }

    private static boolean processAction(String action, BufferedReader br) {
        switch (action.toLowerCase()) {
            case "upload":
                clientUpload(br);
                return false;
            case "download":
                clientDownload(br);
                return false;
            case "modify":
                clientModify(br);
                return false;
            case "delete":
                clientDelete(br);
                return false;
            case "title":
                clientSearchByTitle(br);
                return false;
            case "user":
                clientSearchByUser(br);
                return false;
            case "user-title":
                clientSearchByUserAndTitle(br);
                return false;
            case "exit":
                return true;
            default:
                System.out.println(">>> Sorry, this is not a valid option.");
                return false;
        }
    }

    private static void clientUpload(BufferedReader br) {
        try {
            // Get the path of the file from the user
            System.out.println(">>> Introduce the path of the file that you want to upload:");
            System.out.print("€ ");
            String path = br.readLine();
            // Check if file exists and is not a directory
            File file = new File(path);
            if (!file.exists() || file.isDirectory()) {
                System.out.println(">>> ERROR! Not a valid file path!");
                return;
            }
            // Get the title from the user
            System.out.println(">>> Introduce the title that you want to upload:");
            System.out.print("€ ");
            String title = br.readLine();
            // Get the description from the user
            System.out.println(">>> Introduce the description of your content:");
            System.out.print("€ ");
            String description = br.readLine();
            // Get the file name
            Path p = Paths.get(path);
            String filename = p.getFileName().toString();
            // Get the file's bytes
            byte[] bytes = Files.readAllBytes(p);
            uploadContent(br, title, description, filename, bytes);
        } catch (IOException ex) {
            System.out.println("ERROR reading input!");
        } catch (NotFound n) {
            System.out.println("ERROR: Something wrong happened with the server response");
        }
    }

    private static void uploadContent(BufferedReader br, String title, String description,
                                      String filename, byte[] bytes) throws IOException, NotFound {
        Response response = serviceProvider.uploadInfo(title, description, filename, bytes, clientID);
        if (response.getCode() == 201) {
            // Upload OK
            System.out.println(">>> The uploaded content has received the ID " + response.getMessage());
        }else if (response.getCode() == 409) {
            // Conflict with existent code
            System.out.println(">>> WARNING! You have previously uploaded a content with the same name!");
            manageConflictLoop(br, response.getMessage(), title, description, filename, bytes);
        } else {
            // Unknown error
            System.out.println("ERROR: " + response.getMessage());
        }
    }

    private static void manageConflictLoop(BufferedReader br, String conflictID, String title, String description,
                                           String filename, byte[] bytes) throws IOException, NotFound {
        boolean end = false;
        while (!end) {
            System.out.println(">>> What shall we do with the naming conflict?");
            printConflictActions();
            String action = br.readLine();
            end = processConflictAction(action, br, conflictID, title, description, filename, bytes);
        }
    }

    private static void printConflictActions() {
        System.out.println(">>> (1) Change new resource name");
        System.out.println(">>> (2) Change uploaded resource name");
        System.out.println(">>> (3) Overwrite uploaded resource");
        System.out.println(">>> (4) I don't want to do nothing...");
    }

    private static boolean processConflictAction(String action, BufferedReader br, String conflictID,
                                                 String title, String description, String filename, byte[] bytes)
                                                    throws IOException, NotFound {
        switch (action) {
            case "1":
                changeNewResourceName(br, description, filename, bytes);
                return true;
            case "2":
                changeUploadedResourceName(br, conflictID, title, description, filename, bytes);
                return true;
            case "3":
                overwriteUploadedResource(br, conflictID, title, description, filename, bytes);
                return true;
            case "4":
                System.out.println(">>> The content has not been uploaded");
                return true;
            default:
                System.out.println(">>> Wrong answer. Pleas type 1, 2, 3 or 4");
                return false;
        }
    }

    private static void changeNewResourceName(BufferedReader br, String description,
                                              String filename, byte[] bytes) throws IOException, NotFound {
        System.out.println(">>> INFO: You are going to change the new resource title");
        System.out.println(">>> Introduce the new title:");
        System.out.print("€");
        String newTitle = br.readLine();
        Response response = serviceProvider.uploadInfo(newTitle, description, filename, bytes, clientID);
        if (response.getCode() == 201) {
            // content uploaded
            System.out.println(">>> The uploaded content has received the ID " + response.getMessage());
        } else if (response.getCode() == 409) {
            // Conflict with existent code
            System.out.println(">>> ERROR! There is another conflict with content " + response.getMessage());
            System.out.println(">>> Sorry, you will have to manage this by hand");
        } else {
            // unknown error
            System.out.println("ERROR: " + response.getMessage());
        }
    }

    private static void changeUploadedResourceName(BufferedReader br, String conflictID, String title, String description,
                                                   String filename, byte[] bytes) throws IOException, NotFound {
        System.out.println(">>> INFO: You are going to change the uploaded resource title");
        System.out.println(">>> Introduce the new title:");
        System.out.print("€");
        String newTitle = br.readLine();
        if(modifyContent(conflictID, newTitle, "")) {
            // content has been modified correctly
            uploadContent(br, title, description, filename, bytes);
        }
    }

    private static void overwriteUploadedResource(BufferedReader br, String conflictID, String title,
                                                  String description, String filename, byte[] bytes)
                                                    throws IOException, NotFound {
        System.out.println(">>> INFO: You are going to overwrite the uploaded content. Are you sure?");
        if (affirmativeAnswer(br)) {
            deleteContent(conflictID);
            uploadContent(br, title, description, filename, bytes);
        } else {
            System.out.println(">>> ABORTED: Content not overwritten and not uploaded");
        }
    }

    private static void clientDownload(BufferedReader br) {
        try {
            System.out.println(">>> Introduce the resource ID: ");
            System.out.print("€ ");
            String id = br.readLine();
            InformationInterface info = serviceProvider.download(id);
            System.out.println(info.getStringInfo());
            // Write bytes to file
            if (info.hasBytes()) {
                String filePath = downloadFolder.getAbsolutePath() + "/" + info.getFilename();
                FileOutputStream f = new FileOutputStream(filePath);
                f.write(info.getBytes());
                f.close();
                System.out.println(">>> Saved file to " + filePath);
            }
        } catch (IOException e) {
            System.out.println("ERROR reading input!");
        }
    }

    private static void clientModify(BufferedReader br) {
        try {
            System.out.println(">>> Introduce the resource ID to be modified:");
            System.out.print("€ ");
            String id = br.readLine();
            System.out.println(">>> Introduce the new title:");
            System.out.print("€ ");
            String title = br.readLine();
            System.out.println(">>> Introduce the new description:");
            System.out.print("€ ");
            String description = br.readLine();
            modifyContent(id, title, description);
        } catch (IOException e) {
            System.out.println("ERROR reading input!");
        } catch (NotFound e) {
            System.out.println("ERROR: Something wrong happened with the server response");
        }
    }

    private static boolean modifyContent(String contentID, String title, String description) throws NotFound, RemoteException {
        Response response = serviceProvider.modify(contentID, clientID, title, description);
        JSonFile json = new JSonFile(response.getMessage());
        if (response.getCode() == 409) {
            // conflict with existent content
            System.out.println("CONFLICT: Title already exists in content " + response.getMessage());
            System.out.println(">>> Sorry, you will have to manage this by hand");
            return false;
        }
        if (response.getCode() != 200) {
            // unknown error
            System.out.println("ERROR: " + json.getParam("msg"));
            return false;
        }
        String newTitle = json.getParam("title");
        String newDescription = json.getParam("description");
        System.out.println("+++ New Title: " + newTitle);
        System.out.println("+++ New Description: " + newDescription);
        return true;
    }

    private static void clientDelete(BufferedReader br) {
        try {
            System.out.println(">>> Introduce the resource ID to be deleted:");
            System.out.print("€ ");
            String id = br.readLine();
            System.out.println(">>> The resource will be permanently deleted. Are you sure to continue?");
            if (affirmativeAnswer(br)) {
                deleteContent(id);
            } else {
                System.out.println(">>> Deletion of content " + id + " aborted.");
            }
        } catch (IOException e) {
            System.out.println("ERROR reading input!");
        } catch (NotFound e) {
            System.out.println("ERROR: Something wrong happened with the server response");
        }
    }

    private static void deleteContent(String contentID) throws RemoteException, NotFound {
        Response response = serviceProvider.delete(contentID, clientID);
        if (response.getCode() != 200) {
            JSonFile json = new JSonFile(response.getMessage());
            String msg = json.getParam("msg");
            System.out.println("ERROR: " + msg);
        } else {
            System.out.println("Content " + contentID + " successfully deleted");
        }
    }

    private static boolean affirmativeAnswer(BufferedReader br) throws IOException {
        String response;
        do {
            System.out.println(">>> Write y or n:");
            System.out.print("€ ");
            response = br.readLine();
        } while (!response.equals("y") && !response.equals("n"));
        return response.equals("y");
    }

    private static void clientSearchByTitle(BufferedReader br) {
        try {
            System.out.println(">>> Write the terms to be searched:");
            System.out.print("€ ");
            String terms = br.readLine();
            String [] ids = serviceProvider.search(terms);
            printIDs(ids);
        } catch (IOException e) {
            System.out.println("ERROR reading input!");
        }
    }

    private static void printIDs(String[] ids) {
        if (ids.length == 0) {
            System.out.println("No contents found.");
        }
        for (String id : ids) {
            System.out.println(id);
        }
    }

    private static void clientSearchByUser(BufferedReader br) {
        try {
            System.out.println(">>> Write the username to be searched:");
            System.out.print("€ ");
            String username = br.readLine();
            String [] ids = serviceProvider.searchByUser(username);
            printIDs(ids);
        } catch (IOException e) {
            System.out.println("ERROR reading input!");
        }
    }

    private static void clientSearchByUserAndTitle(BufferedReader br) {
        try {
            System.out.println(">>> Write the exact username to be searched:");
            System.out.print("€ ");
            String username = br.readLine();
            System.out.println(">>> Write the terms to be searched:");
            System.out.print("€ ");
            String terms = br.readLine();
            String[] ids = serviceProvider.search(username, terms);
            printIDs(ids);
        } catch (IOException e) {
            System.out.println("ERROR reading input!");
        }
    }

}
