package server;

import org.omg.CosNaming.NamingContextPackage.NotFound;
import server.managers.ConnectionManager;
import utilities.JSonFile;
import utilities.Response;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {
    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("Usage: ./Server <Server IP> <Server Port> <WebService Address>");
            System.exit(2);
        }
        String ip = args[0];
        int port = Integer.parseInt(args[1]);
        String wsAddress = args[2];
        String[] registerServer;
        System.setProperty("java.rmi.server.hostname", ip);
        try {
//            SecurityManager sec = new SecurityManager();
//            System.setSecurityManager(sec);
            registerServer = registerServer(ip, port, wsAddress);
            ServiceProviderImpl exportedProvidingServices =
                    new ServiceProviderImpl(wsAddress, registerServer[1]);
            startRegistry(port);
            String registryURL = registerServer[0];
            Naming.rebind(registryURL, exportedProvidingServices);
            System.out.println("Server ready");
        } catch (Exception e) {
            System.out.println("Exception initializing the server: "
                    + e.getMessage());
        }

    }

    private static String[] registerServer(String ip, int port, String wsAdress){
        String[] result = new String[2];
        JSonFile json = new JSonFile();
        json.addParam("serverIP", ip);
        json.addParam("serverPort", port);
        Response response = ConnectionManager.post(wsAdress+"/rest/servers", json.makeJSon());
        json = new JSonFile(response.getMessage());
        if(response.isError()){
            try{
                System.out.println(json.getParam("msg"));
            }
            catch(Exception e){
                System.out.println("Something went wrong");
            }
            System.exit(-1);
        }
        System.out.println(response.getMessage());
        try{
            result[0] = json.getParam("serverURL");
            result[1] = json.getParam("serverID");
            return result;
        }
        catch (NotFound e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    private static void startRegistry(int RMIPort)
            throws RemoteException {
        try {
            Registry registry = LocateRegistry.getRegistry(RMIPort);
            registry.list();

        } catch (RemoteException ex) {
            System.out.println("RMI registry cannot be located at port "
                    + RMIPort);
            Registry registry = LocateRegistry.createRegistry(RMIPort);
            System.out.println("RMI registry created at port " + RMIPort);
        }
    }
}
