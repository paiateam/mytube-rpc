package server;

import server.managers.ConnectionManager;
import server.managers.InfoManager;
import server.managers.UsersManager;
import utilities.InformationInterface;
import utilities.Response;
import utilities.ServiceProvider;

import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;
import java.rmi.server.ServerNotActiveException;
import java.rmi.server.UnicastRemoteObject;

public class ServiceProviderImpl extends UnicastRemoteObject
        implements ServiceProvider{

    String wsAdress;
    String serverID;

    public ServiceProviderImpl(String wsAdress, String serverID) throws RemoteException{
        super();
        this.wsAdress = wsAdress;
        this.serverID = serverID;
        UsersManager.SetWSAdress(wsAdress);
        UsersManager.SetServerID(serverID);
        InfoManager.setServerID(serverID);
        InfoManager.setWsAddress(wsAdress);
        ConnectionManager.setWsAdress(wsAdress);
    }

    public Response logIn(String userName, String password) throws RemoteException {
        StringBuilder coveredPassword = new StringBuilder(password.substring(0, 5));
        for(int i = 0; i<password.length()-5; i++){
            coveredPassword.append('*');
        }

        try{
            System.out.println("[" + RemoteServer.getClientHost() + "]: " +
                    "is attempting to logIn with username: " + userName + " password hash: " + coveredPassword);
        }
        catch (ServerNotActiveException e){
            System.out.println(e.getMessage());
        }
        return UsersManager.registerUser(userName, password);
    }

    public Response uploadInfo(String title, String description, String filename, byte[] bytes, String userId)
            throws RemoteException {
        Response response = UsersManager.getUsername(userId);
        if(response.isError()) return response;
        String userName = response.getMessage();
        try{
            System.out.println("[" + RemoteServer.getClientHost() + "]: " +
                    "User <" + userName+ "> is uploading file: -" + title);
        }
        catch (ServerNotActiveException e){
            System.out.println(e.getMessage());
        }
        response = InfoManager.getInstance().uploadInfo(userId, title, description, filename, bytes);

        return response;
    }

    public InformationInterface download(String contentId) throws RemoteException {
        try{
            System.out.println("[" + RemoteServer.getClientHost() + "]: " +
                    "is downloading content: " + contentId);
        }
        catch (ServerNotActiveException e){
            System.out.println(e.getMessage());
        }
        return InfoManager.getInstance().getInfo(contentId);
    }

    public Response modify(String contentId, String userId, String title, String description) throws RemoteException {
        try{
            System.out.println("[" + RemoteServer.getClientHost() + "]: " +
                    "attempting to modify file: " + contentId);
        }
        catch (ServerNotActiveException e){
            System.out.println(e.getMessage());
        }

        return InfoManager.getInstance().modifyInfo(userId, contentId, title, description);

    }

    public Response delete(String contentId, String userId) throws RemoteException {
        try{
            System.out.println("[" + RemoteServer.getClientHost() + "]: " +
                    "is attempting to delete file: " + contentId);
        }
        catch (ServerNotActiveException e){
            System.out.println(e.getMessage());
        }
        return InfoManager.getInstance().deleteInfo(userId, contentId);
//        int code = InfoManager.getInstance().deleteInfo(user, contentId);
//        if (code == -1) {
//            System.out.println("[" + user.getUserName() + "] can not delete content " + contentId);
//        } else {
//            System.out.println("[" + user.getUserName() + "] has deleted content " + contentId);
//        }
//        return code;
    }

    public String[] search(String terms) throws RemoteException {
        try{
            System.out.println("[" + RemoteServer.getClientHost() + "]: " +
                    "is browsing files with terms: " + terms);
        }
        catch (ServerNotActiveException e){
            System.out.println(e.getMessage());
        }

        return InfoManager.search(terms);
    }

    public String[] searchByUser(String username) throws RemoteException {
        try{
            System.out.println("[" + RemoteServer.getClientHost() + "]: " +
                    "is browsing contents uploaded by: " + username);
        }
        catch (ServerNotActiveException e){
            System.out.println(e.getMessage());
        }

        return InfoManager.searchByUser(username);

    }

    public String[] search(String username, String terms) throws RemoteException{
        try{
            System.out.println("[" + RemoteServer.getClientHost() + "]: " +
                    "is browsing contents uploaded by: " + username + " with terms:" +
            terms);
        }
        catch (ServerNotActiveException e){
            System.out.println(e.getMessage());
        }
        return InfoManager.search(username, terms);
    }

}
