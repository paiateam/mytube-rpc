package server;

import java.util.ArrayList;
import java.util.List;

public class User {

    private List<Integer> uploadedInfo;
    private String userName;

    public User(String name){

        this.userName = name;
        uploadedInfo = new ArrayList<>();
    }

    public String getUserName(){
        return this.userName;
    }

    public int userID(){
        return hashCode();
    }

    @Override
    public int hashCode(){
        return userName.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof User) {
            User user = (User) obj;
            return user.userName.equals(this.userName);
        }
        return false;
    }

    public boolean registerUpload(Integer id){
        return uploadedInfo.add(id);
    }

    public boolean deleteUpload(Integer id){
        if(uploadedInfo.contains(id)){
            return uploadedInfo.remove(id);
        }
        return false;
    }

    public int[] getUserContents(){
        int [] results = new int[uploadedInfo.size()];
        for(int i=0; i < uploadedInfo.size(); i++){
            results[i] = uploadedInfo.get(i);
        }
        return results;
    }

    // Only for testing
    public List<Integer> getContentsList() {
        return uploadedInfo;
    }
}
