package server.managers;

import org.omg.CosNaming.NamingContextPackage.NotFound;
import utilities.JSonFile;
import utilities.*;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

public class ConnectionManager {

    private static String wsAdress = "";

    public static Response post(String url, String json) {
        try {
            URL urlDest = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlDest.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes());
            os.flush();
            InputStream is;
            if(conn.getResponseCode() < 400){
                is = conn.getInputStream();
            }
            else{
                is = conn.getErrorStream();
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String output = "";
            String result = "";

            while ((output = br.readLine()) != null) {
                result += output;
            }
            return new Response(conn.getResponseCode(), result);
        } catch (Exception e) {
            return new Response(500, "{\"msg\":\"Connection error\"}");
        }
    }

    public static Response get(String url){
        try {
            URL urlDest = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlDest.openConnection();
            conn.setRequestMethod("GET");

            InputStream is;
            if(conn.getResponseCode() < 400){
                is = conn.getInputStream();
            }
            else{
                is = conn.getErrorStream();
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String output = "";
            String result = "";

            while ((output = br.readLine()) != null) {
                result += output;
            }
            return new Response(conn.getResponseCode(), result);
        } catch (Exception e) {
            return new Response(500, "{\"msg\":\"Connection error\"}");
        }
    }

    public static Response put(String url, String json){
        try {
            URL urlDest = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlDest.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes());
            os.flush();
            InputStream is;
            if(conn.getResponseCode() < 400){
                is = conn.getInputStream();
            }
            else{
                is = conn.getErrorStream();
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String output = "";
            String result = "";

            while ((output = br.readLine()) != null) {
                result += output;
            }
            return new Response(conn.getResponseCode(), result);
        } catch (Exception e) {
            return new Response(500, "{\"msg\":\"Connection error\"}");
        }
    }

    public static Response delete(String url){
        try{
            URL urlDest = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) urlDest.openConnection();
            conn.setRequestMethod("DELETE");
            conn.connect();
            InputStream is;
            if(conn.getResponseCode() < 400){
                is = conn.getInputStream();
            }
            else{
                is = conn.getErrorStream();
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String output = "";
            String result = "";

            while ((output = br.readLine()) != null) {
                result += output;
            }
            return new Response(conn.getResponseCode(), result);
        }
        catch (Exception e){
            return new Response(500, "{\"msg\":\"Connection error\"}");
        }
    }

    public static InformationInterface download(String serverID, String contentID){
        ServiceProvider sp;
        try{
            Response response = get(wsAdress+"/servers/"+serverID);
            if(response.isError())
                return new NullInformation("We are experimenting connection problems, we are working on it, try later");
            JSonFile json = new JSonFile(response.getMessage());
            String url = json.getParam("serverURL");
            sp = (ServiceProvider) Naming.lookup(url);
            return sp.download(contentID);

        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        try {
            return new NullInformation("Error retrieving data from our server, probably down"+
                    ", we are working on it, try later");
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getWsAdress() {
        return wsAdress;
    }

    public static void setWsAdress(String wsAdress) {
        ConnectionManager.wsAdress = wsAdress + "/rest";
    }
}
