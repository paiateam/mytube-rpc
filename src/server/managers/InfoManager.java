package server.managers;

import utilities.JSonFile;
import utilities.Information;
import utilities.InformationInterface;
import utilities.NullInformation;
import utilities.Response;

import java.io.*;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;

public class InfoManager {

    private HashMap<String, String> pathDB;
    private static String pathDBName;// = "pathDB.map";
    private static File storageFolder;
    private static InfoManager instance;
    private static String wsAddress;
    private static String serverID;

    private InfoManager(){
        pathDB = retrievePathDB();
    }

    public static InfoManager getInstance(){
        if(instance == null){
            instance = new InfoManager();
            if (storageFolder.mkdirs() || storageFolder.isDirectory()) {
                System.out.println(">>> Created the Storage folder in " + storageFolder.getAbsolutePath());
            } else {
                System.out.println("ERROR! Storage folder can not be created!");
                System.exit(-1);
            }
        }
        return instance;
    }

    public static void setWsAddress(String wsAddress) {
        InfoManager.wsAddress = wsAddress+"/rest/contents";
    }

    public static void setServerID(String serverID) {
        InfoManager.serverID = serverID;
        storageFolder = new File(serverID);
        pathDBName = serverID + ".map";
    }

    public Response uploadInfo(String userID, String title, String description,
                               String filename, byte[] bytes) throws RemoteException {
        JSonFile json = new JSonFile();
        json.addParam("serverID", serverID);
        json.addParam("userID", userID);
        json.addParam("title", title);
        json.addParam("description", description);
        Response response = ConnectionManager.post(wsAddress, json.makeJSon());
        json = new JSonFile(response.getMessage());

        Response userResponse = UsersManager.getUsername(userID);
        File userFolder = new File(storageFolder.getAbsolutePath() +
                "/" + userResponse.getMessage());
        if (userFolder.mkdirs() || userFolder.isDirectory()) {
            System.out.println(">>> Created the user folder in " + userFolder);
        } else {
            System.out.println("ERROR! User folder can not be created!");
            System.exit(-1);
        }

        String filePath = userFolder.getAbsolutePath() + "/" + filename;
        try{
            FileOutputStream f = new FileOutputStream(filePath);
            f.write(bytes);
            f.close();
            System.out.println(">>> Saved file to " + filePath);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        Response resultResponse = makeResponse(response);
        pathDB.put(response.getMessage(), filePath);
        savePathMap();
        return resultResponse;
    }

    public InformationInterface getInfo(String infoID) throws RemoteException {
        Response response = ConnectionManager.get(wsAddress + "/"+infoID);
        if(response.isError())
            return new NullInformation();
        JSonFile json = new JSonFile(response.getMessage());
        try{
            String focus_serverID = json.getParam("serverID");
            if(focus_serverID.equals(serverID)) {
                if (pathDB.containsKey(infoID)) {
                    String contentID = json.getParam("contentID");
                    String userID = json.getParam("userID");
                    String title = json.getParam("title");
                    String description = json.getParam("description");
                    String path = pathDB.get(infoID);
                    File file = new File(path);
                    if (!file.exists() || file.isDirectory()) {
                        System.out.println(">>> ERROR! Not a valid file path!");
                        return new NullInformation();
                    }
                    Path p = Paths.get(path);
                    String filename = p.getFileName().toString();
                    byte[] bytes = Files.readAllBytes(p);
                    response = UsersManager.getUsername(userID);
                    String username = response.getMessage();
                    return new Information(username, title, description, filename, bytes);
                } else {
                    return new NullInformation();
                }
            }
            else{
                return ConnectionManager.download(focus_serverID, infoID);
            }
        }
        catch (Exception e){
            return new NullInformation();
        }
    }

    public Response deleteInfo(String userID, String infoID) throws RemoteException {
        return ConnectionManager.delete(wsAddress +"/"+infoID+"?userID="+userID);
    }

    public Response modifyInfo(String userID, String infoID, String newTitle, String newInfo) throws RemoteException {
        if (newTitle.equals("") && newInfo.equals("")) {
            return new Response(400, "If you don't want to modify nothing, why are you trolling the program? >:(");
        }

        JSonFile json = new JSonFile();
        if(!newTitle.equals("")){
            json.addParam("title", newTitle);
        }
        if(!newInfo.equals("")){
            json.addParam("description", newInfo);
        }
        return ConnectionManager.put(wsAddress +"/"+infoID+"?userID="+userID, json.makeJSon());
    }

    public static String[] search(String terms){
        try{
            String searchCode = wsAddress + "/search?terms=" + URLEncoder.encode(terms, "UTF-8");
            return searchURL(searchCode);
        }
        catch (Exception e){
            return new String[0];
        }
    }

    public static String[] searchByUser(String username){
        Response response = UsersManager.getID(username);
        if(response.isError()){
            System.out.println("Error getting user "+username+" Error : "+response.getCode() + ": "
                    + response.getMessage());
            return new String[0];
        }
        String searchCode = wsAddress + "/search?userID="+response.getMessage();
        return searchURL(searchCode);
    }

    public static String[] search(String username, String terms){
        Response response = UsersManager.getID(username);
        if(response.isError()){
            System.out.println("Error getting user "+username+" Error : "+response.getCode() + ": "
                    + response.getMessage());
            return new String[0];
        }
        String searchCode = wsAddress + "/search?userID="+response.getMessage();
        try{
            searchCode += "&terms=" + URLEncoder.encode(terms, "UTF-8");
            return searchURL(searchCode);
        }
        catch (Exception e){
            return new String[0];
        }
    }

    private static String[] searchURL(String url){
        try{
            Response response = ConnectionManager.get(url);
            if(response.isError()){
                System.out.println("Error: " + response.getCode() +": " + response.getMessage());
                return new String[0];
            }
            return listToArray(JSonFile.readList(response.getMessage()));
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return new String[0];
        }
    }

    private static String[] listToArray(List<String> idList){
        String[] result = new String[idList.size()];
        for(int i = 0; i < idList.size(); i++){
            result[i] = idList.get(i);
        }
        return result;
    }

    private Response makeResponse(Response response){
        JSonFile json = new JSonFile(response.getMessage());
        String param;
        if(response.isError()){
            return makeResponseError(response.getCode(), json);
        }
        else{
            try{
                response.setMessage(json.getParam("contentID"));
            }
            catch (Exception e){
                response.setCode(500);
                response.setMessage("Server Error");
            }
        }
        return response;
    }

    private Response makeResponseError(int error, JSonFile json){
        Response response = new Response(error, "");
        try{
            response.setMessage(json.getParam("msg"));
        }
        catch (Exception e){
            response.setMessage("Something went wrong during logIn");
        }
        return response;
    }

    private HashMap<String, String> retrievePathDB(){
        File file = new File(pathDBName);
        if (!file.exists()) {
            return new HashMap<>();
        }
        return deserializeMap();
    }

    private void savePathMap(){
        try
        {
            FileOutputStream fos = new FileOutputStream(pathDBName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(pathDB);
            oos.close();
            fos.close();
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    private HashMap<String, String> deserializeMap(){
        HashMap<String, String> map = null;
        try
        {
            FileInputStream fis = new FileInputStream(pathDBName);
            ObjectInputStream ois = new ObjectInputStream(fis);
            map = (HashMap) ois.readObject();
            ois.close();
            fis.close();
            return map;
        }catch (Exception e){
            System.out.println(e.getMessage());
            return new HashMap<>();
        }
    }
}
