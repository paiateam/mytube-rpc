package server.managers;

import utilities.JSonFile;
import utilities.Response;

public class UsersManager {

    private static String wsAdress;
    private static String serverID;

    public static void SetWSAdress(String wsAdress){

        UsersManager.wsAdress = wsAdress + "/rest/users";
        System.out.println("Updated Users Adress: " + UsersManager.wsAdress);
    }

    public static void SetServerID(String serverID){ UsersManager.serverID = serverID; }

    public static Response registerUser(String userName, String password){

        JSonFile json = new JSonFile();
        json.addParam("userName", userName);
        json.addParam("password", password);
        json.addParam("serverID", serverID);

        Response response = ConnectionManager.post(wsAdress, json.makeJSon());
        return makeResponse(response, false);

    }

    public static Response getID(String username){
        Response response = ConnectionManager.get(wsAdress + "/search?userName=" + username);
        JSonFile json = new JSonFile(response.getMessage());
        return makeResponse(response, false);
    }

    public static Response getUsername(String id){
        Response response = ConnectionManager.get(wsAdress + "/" + id);
        JSonFile json = new JSonFile(response.getMessage());
        return makeResponse(response, true);
    }

    private static Response makeResponse(Response response, boolean isUsername){
        JSonFile json = new JSonFile(response.getMessage());
        String param;
        if(isUsername) param = "userName";
        else param = "userID";
        if(response.isError()){
            return makeResponseError(response.getCode(), json);
        }
        else{
            try{
                response.setMessage(json.getParam(param));
            }
            catch (Exception e){
                response.setCode(500);
                response.setMessage("Server Error");
            }
        }
        return response;
    }

    private static Response makeResponseError(int error, JSonFile json){
        Response response = new Response(error, "");
        try{
            response.setMessage(json.getParam("msg"));
        }
        catch (Exception e){
            response.setMessage("Something went wrong during logIn");
        }
        return response;
    }

}
