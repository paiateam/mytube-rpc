package test.server;

import org.junit.Before;
import org.junit.Test;
import server.managers.InfoManager;
import server.User;
import utilities.Information;
import utilities.NullInformation;

import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class InfoManagerTest {
    private InfoManager iManager;

//    @Before
//    public void setupInfoManagerTest() {
//        iManager = InfoManager.getInstance();
//    }
//
//    @Test
//    public void test_uploadInfo_returns_info_hash_code() throws Exception {
//        User user = new User("username1");
//        String title = "title1";
//        String description = "description1";
//        Information info = new Information(user.getUserName(), title, description);
//        int id = iManager.uploadInfo(user, title, description);
//        assertEquals(info.hashCode(), id);
//    }
//
//    @Test
//    public void test_uploadInfo_returns_minus_1_if_info_already_exists() throws Exception {
//        User user = new User("username2");
//        String title = "title2";
//        String description = "description2";
//        int id1 = iManager.uploadInfo(user, title, description);
//        int id2 = iManager.uploadInfo(user, title, description);
//        assertNotEquals(-1, id1);
//        assertEquals(-1, id2);
//    }
//
//    @Test
//    public void test_getInfo_returns_information_if_it_exists() throws Exception {
//        User user = new User("username3");
//        String title = "title3";
//        String description = "description3";
//        Information info = new Information(user.getUserName(), title, description);
//        int id = iManager.uploadInfo(user, title, description);
//        assertNotEquals(-1, id);
//        assertEquals(info, iManager.getInfo(id));
//    }
//
//    @Test
//    public void test_getInfo_returns_instance_of_NullInfo_if_info_does_not_exists() throws Exception {
//        assertTrue(iManager.getInfo(1234) instanceof NullInformation);
//    }
//
//    @Test
//    public void test_deleteInfo_deletes_info_if_exist_and_belongs_to_user() throws Exception {
//        User user = new User("username4");
//        String title = "title4";
//        String description = "description4";
//        int id = iManager.uploadInfo(user, title, description);
//        assertNotEquals(-1, id);
//        int code = iManager.deleteInfo(user, id);
//        assertNotEquals(-1, code);
//        assertTrue(iManager.getInfo(id) instanceof NullInformation);
//    }
//
//    @Test
//    public void test_deleteInfo_does_not_delete_if_info_does_not_exist() throws Exception {
//        User user = new User("username");
//        int code = iManager.deleteInfo(user, 1234);
//        assertEquals(-1, code);
//    }
//
//    @Test
//    public void test_deleteInfo_does_not_delete_if_info_does_not_belongs_to_user() throws Exception {
//        User validUser = new User("valid-user");
//        String title = "title5";
//        String description = "description5";
//        int id = iManager.uploadInfo(validUser, title, description);
//        User invalidUser = new User("invalid-user");
//        int code = iManager.deleteInfo(invalidUser, id);
//        assertEquals(-1, code);
//        assertFalse(iManager.getInfo(id) instanceof NullInformation);
//    }
//
//    @Test
//    public void test_modifyInfo_modifies_info_if_exist_and_belongs_to_user() throws Exception {
//        User user = new User("username6");
//        String title = "title6";
//        String description = "description6";
//        int id = iManager.uploadInfo(user, title, description);
//        assertNotEquals(-1, id);
//        String newDescription = "new-description6";
//        int code = iManager.modifyInfo(user, id, newDescription);
//        assertEquals(1, code);
//        assertEquals(newDescription, iManager.getInfo(id).getDescription());
//    }
//
//    @Test
//    public void test_modifyInfo_does_not_modify_if_info_does_not_exist() throws Exception {
//        User user = new User("username7");
//        int noID = 1234;
//        String newDescription = "description7";
//        assertEquals(-1, iManager.modifyInfo(user, noID, newDescription));
//    }
//
//    @Test
//    public void test_modifyInfo_does_not_modify_if_info_does_not_belongs_to_user() throws Exception {
//        User validUser = new User("valid-user2");
//        String title = "title8";
//        String description = "description8";
//        int id = iManager.uploadInfo(validUser, title, description);
//        assertNotEquals(-1, id);
//        User invalidUser = new User("invalid-user2");
//        String newDescription = "new-description8";
//        int code = iManager.modifyInfo(invalidUser, id, newDescription);
//        assertEquals(-1, code);
//        assertNotEquals(newDescription, iManager.getInfo(id).getDescription());
//    }
//
//    @Test
//    public void test_search_returns_array_of_ids_of_found_terms() throws Exception {
//        User user = new User("username9");
//        int id1 = iManager.uploadInfo(user, "test", "description");
//        assertNotEquals(-1, id1);
//        int id2 = iManager.uploadInfo(user, "test search", "description");
//        assertNotEquals(-1, id2);
//        int id3 = iManager.uploadInfo(user, "Test", "description");
//        assertNotEquals(-1, id3);
//        int id4 = iManager.uploadInfo(user, "testify", "description");
//        assertNotEquals(-1, id4);
//        int id5 = iManager.uploadInfo(user, "not in array", "description");
//        assertNotEquals(-1, id5);
//        int[] ids = iManager.search("TeSt");
//        assertEquals(4, ids.length);
//        assertTrue(IntStream.of(ids).anyMatch(x -> x == id1));
//        assertTrue(IntStream.of(ids).anyMatch(x -> x == id2));
//        assertTrue(IntStream.of(ids).anyMatch(x -> x == id3));
//        assertTrue(IntStream.of(ids).anyMatch(x -> x == id4));
//        assertFalse(IntStream.of(ids).anyMatch(x -> x == id5));
//    }
}