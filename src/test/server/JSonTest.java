package test.server;

import org.junit.Assert;
import org.junit.Test;
import utilities.JSonFile;

import javax.swing.text.html.HTMLDocument;
import java.util.LinkedList;
import java.util.List;

public class JSonTest {
    @Test
    public void create_JSon_get_Value(){
        JSonFile json = new JSonFile();
        json.addParam("key", "value");
        System.out.println(json.makeJSon());
        try{
            Assert.assertTrue(json.getParam("key") == "value");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void create_JSon_Get_IntValue(){
        JSonFile json = new JSonFile();
        json.addParam("key", 1234);
        System.out.println(json.makeJSon());
        try{
            Assert.assertTrue(json.getIntParam("key") == 1234);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void import_JSon_And_Check_Equality(){
        JSonFile jsonToImport = new JSonFile();
        jsonToImport.addParam("StringValue1", "value1");
        jsonToImport.addParam("IntValue1", 1);
        jsonToImport.addParam("StringValue2", "value2");
        System.out.println("Json to import: ");
        System.out.println(jsonToImport.makeJSon());
        JSonFile importedJson = new JSonFile(jsonToImport.makeJSon());
        System.out.println("Imported Json: ");
        System.out.println(importedJson.makeJSon());
        try{
            Assert.assertTrue(importedJson.getParam("StringValue1").equals("value1"));
            Assert.assertTrue(importedJson.getParam("StringValue2").equals("value2"));
            Assert.assertTrue(importedJson.getIntParam("IntValue1") == 1);
            Assert.assertTrue(importedJson.makeJSon().equals(jsonToImport.makeJSon()));
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void import_Json_List(){
        String exampleList = "{\"contents\":[{\"contentID\":\"id\"}, {\"contentID\":\"id2\"}]}";
        List<String> parsedList = JSonFile.readList(exampleList);
        for (String s: parsedList) {
            Assert.assertTrue(s.equals("id") || s.equals("id2"));
        }
    }

}
