package test.server;

import org.junit.Before;
import org.junit.Test;
import server.User;

import java.util.List;

import static org.junit.Assert.*;

public class UserTest {

    private User user;
    private String username;

    @Before
    public void initUserTest() {
        username = "username";
        user = new User(username);
    }

    @Test
    public void getUserName() throws Exception {
        assertEquals(username, user.getUserName());
    }

    @Test
    public void registerUpload() throws Exception {
        int id = 1234;
        assertTrue(user.registerUpload(id));
        List<Integer> list = user.getContentsList();
        assertTrue(list.contains(id));
    }

    @Test
    public void deleteUpload() throws Exception {
        int id = 1234;
        assertTrue(user.registerUpload(id));
        assertTrue(user.deleteUpload(id));
        List<Integer> list = user.getContentsList();
        assertFalse(list.contains(id));
    }

    @Test
    public void delete_of_non_existing_content_does_not_delete_anything() throws Exception {
        int id = 1234;
        int falseID = 4321;
        assertTrue(user.registerUpload(id));
        assertFalse(user.deleteUpload(falseID));
        List<Integer> list = user.getContentsList();
        assertTrue(list.contains(id));
    }

    @Test
    public void getUserContents() throws Exception {
        int id1 = 1;
        int id2 = 2;
        assertTrue(user.registerUpload(id1));
        assertTrue(user.registerUpload(id2));
        List<Integer> list = user.getContentsList();
        int[] array = user.getUserContents();
        assertTrue(list.size() == array.length);
        assertTrue(list.contains(array[0]));
        assertTrue(list.contains(array[1]));
    }

}