package utilities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Information extends UnicastRemoteObject implements InformationInterface {

    private String username;
    private String title;
    private String description;
    private String filename;
    private byte[] bytes;

    public Information(String username, String title, String description,
                       String filename, byte[] bytes) throws RemoteException {
        super();
        this.username = username;
        this.title = title;
        this.description = description;
        this.filename = filename;
        this.bytes = bytes;
    }

    public int hashCode() {
        int hashCode = 1;
        hashCode = 31 * hashCode + (username.hashCode());
        hashCode = 31 * hashCode + (title.hashCode());
        return hashCode;
    }

    public boolean equals(Object obj) {
        if(obj instanceof Information){
            Information info = (Information) obj;
            return (this.username.equals(info.username)) &&
                    (this.title.equals((info.title)));
        }
        return false;
    }

    public String getUserName() throws RemoteException {
        return username;
    }

    public String getTitle() throws RemoteException {
        return title;
    }

    public String getDescription() throws RemoteException {
        return description;
    }

    public void setDescription(String newInfo) throws RemoteException {
        this.description = newInfo;
    }

    public String getFilename() throws RemoteException{
        return filename;
    }

    public byte[] getBytes() throws RemoteException{
        return bytes;
    }

    public boolean hasBytes() throws RemoteException {
        return true;
    }

    public String getStringInfo() throws RemoteException {
        String s = "";
        s += "User Name: " + username + "\n";
        s += "Title: " + title + "\n";
        s += "Info: " + description + "\n";
        s += "Filename: " + filename + "\n";
        return s;
    }
}
