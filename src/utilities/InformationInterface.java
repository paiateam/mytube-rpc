package utilities;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InformationInterface extends Remote {
    String getUserName() throws RemoteException;
    String getTitle() throws RemoteException;
    String getDescription() throws RemoteException;
    void setDescription(String newInfo) throws RemoteException;
    String getStringInfo() throws RemoteException;
    String getFilename() throws RemoteException;
    byte[] getBytes() throws RemoteException;
    boolean hasBytes() throws RemoteException;
}
