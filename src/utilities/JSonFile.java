package utilities;

import jdk.nashorn.internal.runtime.StoredScript;
import org.omg.CosNaming.NamingContextPackage.NotFound;

import java.lang.reflect.Array;
import java.util.*;

public class JSonFile {

    String jsonCode = "";
    Map<String, String> map_strings;
    Map<String, Integer> map_integers;

    public JSonFile(){
        map_integers = new HashMap<>();
        map_strings = new HashMap<>();
    }

    public JSonFile(String json){
        map_integers = new HashMap<>();
        map_strings = new HashMap<>();
        parseJSon(json);
    }

    public static ArrayList<String> readList(String json){
        JSonFile jsonTemp = new JSonFile();
        return jsonTemp.parseList(json);
    }

    public void addParam(String key, String value){
        map_strings.put(key, value);
        addSeparator();
        jsonCode += "\""+key+"\":\""+value+"\"";
    }

    public void addParam(String key, int value){
        map_integers.put(key, value);
        addSeparator();
        jsonCode += "\""+key+"\":"+value+"";
    }

    public String getParam(String key) throws NotFound{
        if(map_strings.containsKey(key))
            return map_strings.get(key);
        else
            throw new NotFound();
    }

    public int getIntParam(String key) throws NotFound{
        if(map_integers.containsKey(key))
            return map_integers.get(key);
        else
            throw new NotFound();
    }

    public String makeJSon(){
        return jsonCode+"}";
    }

    private void addSeparator(){
        if(jsonCode == ""){
            jsonCode += "{";
        }
        else{
            jsonCode += ",";
        }
    }

    /*Read List*/

    private ArrayList<String> parseList(String json){
        String key = "contents";
        String keyContent = "contentID";
        ArrayList<String> result = new ArrayList<>();
        String readKey, readKeyContent;
        StorageStruct tempStruct;
        int currentIndex = movePointer(json, 0);
        tempStruct = readString(json, currentIndex+1);
        if(!tempStruct.value.equals(key)){
            return result;
        }
        currentIndex = movePointer(json, tempStruct.index+1);
        while(currentIndex != -1){
            tempStruct = readString(json, currentIndex+1);
            if(!tempStruct.value.equals(keyContent)){
                return result;
            }
            currentIndex = tempStruct.index+2;
            tempStruct = readString(json, currentIndex+1);
            result.add(tempStruct.value);
            currentIndex = movePointer(json, tempStruct.index+2);
        }
        return result;
    }

    /*Read and parse JSon logic*/

    private void parseJSon(String json){
        int currentIndex = movePointer(json, 0);
        StorageStruct result;
        String key = "";
        while(currentIndex != -1){
            //Reading key
            result = readString(json, currentIndex+1);
            key = result.value;
            currentIndex = result.index+2;
            //Read Value
            if(json.charAt(currentIndex) == '\"'){
                result = readString(json, currentIndex+1);
                this.addParam(key, result.value);
            }
            else{
                result = readInt(json, currentIndex);
                this.addParam(key, result.valueInt);
            }
            currentIndex = result.index;
            currentIndex = movePointer(json, currentIndex+1);
        }
    }

    private int movePointer(String json, int index){
        while(json.charAt(index) != '\"' && json.charAt(index) != '}'){
            index++;
        }
        if(json.charAt(index) == '\"'){
            return index;
        }
        else{
            return -1;
        }
    }

    //Both readers place the "pointer" one position before the comma.
    private StorageStruct readString(String json, int index){
        StorageStruct result = new StorageStruct();
        while(json.charAt(index) != '\"'){
            result.value += json.charAt(index);
            index++;
        }
        result.index = index;
        return result;
    }

    private StorageStruct readInt(String json, int index){
        StorageStruct result = new StorageStruct();
        while(json.charAt(index) != ','){
            result.value += json.charAt(index);
            index++;
        }
        result.valueInt = Integer.parseInt(result.value);
        result.index = index;
        return result;
    }

    private class StorageStruct{
        protected int index;
        protected String value;
        protected int valueInt;

        public StorageStruct(){
            value = "";
        }
    }

}
