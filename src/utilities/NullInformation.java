package utilities;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class NullInformation extends UnicastRemoteObject implements InformationInterface {

    private final String message;

    public NullInformation() throws RemoteException {
        this.message = "Warning: ID does not exist";
    }

    public NullInformation(String message) throws RemoteException{
        this.message = message;
    }

    @Override
    public String getUserName() throws RemoteException {
        return "";
    }

    @Override
    public String getTitle() throws RemoteException {
        return "";
    }

    public String getDescription() throws RemoteException {
        return "";
    }

    public void setDescription(String newInfo) throws RemoteException {

    }

    @Override
    public String getStringInfo() throws RemoteException {
        return this.message;
    }

    @Override
    public String getFilename() throws RemoteException {
        return "";
    }

    @Override
    public byte[] getBytes() throws RemoteException {
        return new byte[0];
    }

    public boolean hasBytes() throws RemoteException {
        return false;
    }
}
