package utilities;

import java.io.Serializable;

public class Response implements Serializable{
    private int code;
    private String message;

    public Response(int code, String message){
        this.code = code;
        this.message = message;
    }

    public boolean isError(){
        return code >= 400;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
