package utilities;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServiceProvider extends Remote {
    Response logIn(String username, String password) throws RemoteException;
    Response uploadInfo(String title, String description, String filename, byte[] bytes, String userId) throws RemoteException;
    InformationInterface download(String contentId) throws RemoteException;
    Response modify(String contentId, String userId, String title, String description) throws RemoteException;
    Response delete(String contentId, String userId) throws RemoteException;
    String[] search(String term) throws RemoteException;
    String[] searchByUser(String username) throws RemoteException;
    String[] search(String username, String terms) throws RemoteException;
}
